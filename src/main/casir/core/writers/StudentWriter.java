package casir.core.writers;

import casir.dataTransferObject.Student;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class StudentWriter
{
    public static void WriteStudents(List<Student> studentsToWrite)
    {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            // root element
            Element rootElement = doc.createElement("students");
            doc.appendChild(rootElement);

            // Student element
            for(Student studentToWrite : studentsToWrite)
            {
                Element student = doc.createElement("student");
                Attr id = doc.createAttribute("id");
                id.setValue(studentToWrite.getiD());
                student.setAttributeNode(id);
                Attr firstName = doc.createAttribute("firstName");
                firstName.setValue(studentToWrite.getFirstName());
                student.setAttributeNode(firstName);
                Attr lastName = doc.createAttribute("lastName");
                lastName.setValue(studentToWrite.getLastName());
                student.setAttributeNode(lastName);
                Attr group = doc.createAttribute("group");
                group.setValue(studentToWrite.getGroup());
                student.setAttributeNode(group);
                rootElement.appendChild(student);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            Path currentRelativePath = Paths.get("");
            String pathString = currentRelativePath.toAbsolutePath().toString();
            StreamResult result = new StreamResult(new File(pathString+"/Database/StudentDB.xml"));
            transformer.transform(source, result);

            //DEBUG PURPOSES
            System.out.println("XML Writing success");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
