package casir.core.readers;

import casir.dataTransferObject.Student;
import casir.core.writers.StudentWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class StudentDatabaseReader
{
    public static List<Student> ReadStudents()
    {

    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    List<Student> studentsList = null;
    try
    {
    SAXParser saxParser = saxParserFactory.newSAXParser();
    DatabaseReaderHandler handler = new DatabaseReaderHandler();

    Path currentRelativePath = Paths.get("");
    String pathString = currentRelativePath.toAbsolutePath().toString();
    saxParser.parse(new File(pathString+"/Database/StudentDB.xml"), handler);
    //Get Students list
    studentsList = handler.getStudents();
    }
    catch (FileNotFoundException e)
    {
        StudentWriter.WriteStudents(new ArrayList<Student>());
        return new ArrayList<Student>();

    }
    catch (ParserConfigurationException | IOException | org.xml.sax.SAXException e)
    {
        e.printStackTrace();
    }

    return studentsList;
    }
}
