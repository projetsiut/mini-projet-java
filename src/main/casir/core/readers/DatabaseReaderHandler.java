package casir.core.readers;

import casir.dataTransferObject.Student;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;

import java.util.ArrayList;
import java.util.List;


public class DatabaseReaderHandler extends DefaultHandler
{
    private ArrayList<Student> studentList = new ArrayList<>();
    private Student student;

    public List<Student> getStudents()
    {
        return studentList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("student"))
        {
            // create a new Student and put it in Map
            String id = attributes.getValue("id");
            String firstName = attributes.getValue("firstName");
            String lastName = attributes.getValue("lastName");
            String group = attributes.getValue("group");
            // initialize Student object and set id attribute
            student = new Student(id,firstName,lastName,group);
            studentList.add(student);
        }

    }

}
