package casir.core.server;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * This class is used to serve file to HTTP request
 */
public class FileServer
{

    /**
     * Check if given request it about a file
     * @param requestURI URI - The uri to check
     * @return Wheter or not given URI correspond to a file
     */
    public boolean isFile(URI requestURI){
        String[] extension = requestURI.getPath().split("[.]");
        return extension.length > 1;
    }

    /**
     * Serve the file
     * Find the corresponding file and send it or throw an exception
     * @param requestURI URI - URI of the file to be served
     */
    public ArrayList<String> serveFile(URI requestURI) throws FileNotFoundException
    {
        ArrayList<String> result = new ArrayList<>();
        // Remove first slash
        File file = new File(requestURI.getPath().substring(1));

        if (file.isFile()){
            try
            {
                result.add(this.getFullFile(file));
            } catch (IOException e)
            {
                System.err.println("Error while reading file");
                e.printStackTrace();
            }

            result.add(this.getMimeType(requestURI));

            return result;
        } else {
            throw new FileNotFoundException();
        }
    }

    /**
     * @param requestURI URI - The uri where to extract file mime type
     * @return String - The MIME type if not in list return text/plain
     */
    private String getMimeType(URI requestURI)
    {
        String[] filePath = requestURI.getPath().split("[.]");
        System.err.println("- found filte type : " + filePath[filePath.length - 1]);

        switch (filePath[filePath.length - 1]){
            case "png":
                return "image/png;";
            case "gif":
                return "image/gif;";
            case "jpeg":
                return "image/jpeg;";
            case "jpg":
                return "image/jpeg;";
            case "svg":
                return "image/svg+xml;";
            case "css":
                return "text/css;";
            case "js":
                return "text/javascript;";
            default:
                    return "text/plain;";
        }
    }

    /**
     * Return the whole content of a file
     * @param file File - The file to be dump
     * @return String - The content of the file
     * @throws IOException --
     */
    private String getFullFile(File file) throws IOException
    {
        byte[] data = new byte[(int) file.length()];
        FileInputStream fileInputStream = new FileInputStream(file);
        fileInputStream.read(data);
        return new String(data, StandardCharsets.UTF_8);
    }

}
