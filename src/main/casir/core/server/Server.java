package casir.core.server;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import casir.core.server.route.Route;
import casir.core.server.route.RouteParser;
import com.sun.net.httpserver.HttpServer;

public class Server
{

    public void start() throws Exception
    {
        ArrayList<Route> routes = RouteParser.parse();

        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/", new Router(routes));
        server.setExecutor(null); // creates a default executor
        server.start();
    }
}