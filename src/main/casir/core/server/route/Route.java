package casir.core.server.route;

import casir.core.Controller;

/**
 * This class define a new route for http server with its associated Controller class
 */
public class Route
{
    private String context;
    private Controller controller;
    private String controllerName;
    private String method;


    public Route(String context, String controllerName, String method)
    {
        this.context = context;
        this.method = (method == null)? "index" : method;
        this.controllerName = controllerName;
        this.getControllerInstance();
    }

    private void getControllerInstance()
    {
        try {
//            Instanciate a new controller class by it's name
            Class<?> controllerClass = Class.forName("casir.controller."+this.controllerName);
            this.controller = (Controller) controllerClass.newInstance();

        } catch (ClassNotFoundException e) {
            System.err.println("Class not found");
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public String getContext()
    {
        return context;
    }

    public Controller getController()
    {
        return controller;
    }

    public String getMethod()
    {
        return this.method;
    }

    @Override
    public String toString()
    {
        return "[" + this.getContext() + "||" + this.getController() + "||" + this.getMethod() + "]";
    }
}
