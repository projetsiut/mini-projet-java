package casir.core.server.route;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;

public class RouteParser
{
    public static ArrayList<Route> parse(){
        ArrayList<Route> routes = new ArrayList<>();

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            Handler handler = new Handler();

            parser.parse("config/Route.xml", handler);
            routes = handler.getRoutes();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return routes;
    }
}
