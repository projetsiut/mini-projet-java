package casir.core.server.route;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class Handler extends DefaultHandler
{
    private ArrayList<Route> routes;

    public Handler()
    {
        this.routes = new ArrayList<>();
    }

    public void startElement(String namespaceURI, String lname, String qname, Attributes attrs) throws SAXException
    {

//      Only route element are useful
        if(qname.equals("route")){
            String context, controller, method;

            context = attrs.getValue("context");
            controller = attrs.getValue("controller");
            method = attrs.getValue("method");

            this.routes.add(new Route(context, controller, method));
        }
    }

    public ArrayList<Route> getRoutes(){
        return this.routes;
    }
}
