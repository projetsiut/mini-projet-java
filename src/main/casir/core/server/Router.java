package casir.core.server;

import casir.core.server.route.Route;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is used just between the HTTP server and the controller.
 * It allow request to be routed to the corresponding controller or file to be served with the FileServer
 */
public class Router implements HttpHandler {

    private static final int BUFFER_SIZE = 400;

    private HashMap<String, Route> routes;
    private FileServer fileServer;

    public Router(ArrayList<Route> routes) {
        this.fileServer = new FileServer();
        this.routes = new HashMap<>();

        System.err.println("Router initilization : ");
        for(Route route : routes){
            this.routes.put(route.getContext(), route);
            System.err.println("- found route : " + route.getContext() + ", " + route.getController() + ":" + route.getMethod());
        }
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException
    {
        System.err.println("HTTP - Request");
        if (this.fileServer.isFile(httpExchange.getRequestURI())){
            ArrayList<String> fileData;

            try {
                fileData = this.fileServer.serveFile(httpExchange.getRequestURI());
                this.sendResponse(200, fileData.get(0), fileData.get(1), httpExchange);
                System.err.println("- sent file : " + httpExchange.getRequestURI().getPath());

            } catch (FileNotFoundException e){
                this.sendResponse(404, "404 - Not Found.", httpExchange);
                System.err.println("- ERROR : not sent : " + httpExchange.getRequestURI().getPath());
            }
        } else {
            Route route = this.getRoute(httpExchange.getRequestURI());

            System.err.println("- used route : " + route);

            if (route == null) {
                System.err.println("- ERROR : null route found");
                this.sendResponse(404, "404 - Not Found.", httpExchange);
            }
            else
            {
                System.err.println("- query string : " + httpExchange.getRequestURI().getQuery());
                String response = this.invokeController(route, this.getParameters(httpExchange.getRequestURI().getQuery()));

                if (response != null){
                    this.sendResponse(200, response, httpExchange);
                }
                else{
                    System.err.println("- ERROR : null response found");
                    this.sendResponse(404, "404 - Not Found", httpExchange);
                }
            }
        }
    }

    /**
     * The method name of the route is called on the controller, then result is return
     * Controller method must return a String
     * @param route Route - The route where data is holded
     * @return String - The parsed html page
     */
    private String invokeController(Route route, Map<String, String> GET)
    {
        String result;
        try {
            Class[] args = new Class[1];
            args[0] = Map.class;

            // Get method from the controller by it's name, null is the parameters here we know that there is no parameter
            Method controllerMethod = route.getController().getClass().getMethod(route.getMethod(), args);
            // Invoke the method and get it's result, null is the args too so there is no args.
            result = (String) controllerMethod.invoke(route.getController(), GET);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            System.err.println("- EXCEPTION : ");
            e.printStackTrace();
            return null;
        }

        return result;
    }

    /**
     * Send HTTP response to the client
     * @param status int - The status to be returned commonly : 200, 302, 404,..
     * @param response String - The html page to return
     * @param httpExchange HttpExchange - http exchange data for sending headers, ...
     */
    private void sendResponse(int status, String response, HttpExchange httpExchange)
    {
        try{
            System.err.println("- sending response : " + status + ", length : " + response.length());

            Headers headers = httpExchange.getResponseHeaders();
            // Buffer for sending response
            BufferedOutputStream out = new BufferedOutputStream(httpExchange.getResponseBody());
            ByteArrayInputStream bis = new ByteArrayInputStream(response.getBytes());

            // Send headers
            headers.set("Content-Type", "text/html; charset=utf-8");
            httpExchange.sendResponseHeaders(status, response.getBytes().length);

            // Send response
            byte[] buffer = new byte[BUFFER_SIZE];
            int count;
            while ((count = bis.read(buffer)) != -1) {
                out.write(buffer, 0, count);
            }

            System.err.println("- sent response body");
            out.flush();
            out.close();
            bis.close();

        } catch (IOException e){
            System.err.println("- EXCEPTION : ");
            e.printStackTrace();
        }


    }

    /**
     * Send HTTP response to the client
     * @param status int - The status to be returned commonly : 200, 302, 404,..
     * @param response String - The html page to return
     * @param contentType String - The MIME type of the file
     * @param httpExchange HttpExchange - http exchange data for sending headers, ...
     * @throws IOException --
     */
    public void sendResponse(int status, String response, String contentType, HttpExchange httpExchange) throws IOException
    {
        System.err.println("- sending response : " + status + ", length : " + response.length());
        Headers headers = httpExchange.getResponseHeaders();

        headers.set("Content-Type", contentType);
        httpExchange.sendResponseHeaders(status, response.length());

        OutputStream os = httpExchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    /**
     * @param requestURI URI - Define the route to be searched
     * @return Route - The route associated with the uri in config/Route.xml file
     */
    private Route getRoute(URI requestURI) {
        System.err.println("- requestURI : " + requestURI.getPath());
        return this.routes.getOrDefault(requestURI.getPath(), null);
    }

    /**
     * Parse the query to get GET parameter
     * @param query String
     * @return Retun GET parameters as a HashMap
     */
    public Map<String, String> getParameters(String query) {
        Map<String, String> result = new HashMap<>();
        if (query != null){
            for (String param : query.split("&")) {
                String[] entry = param.split("=");
                if (entry.length > 1) {
                    result.put(entry[0], entry[1]);
                }else{
                    result.put(entry[0], "");
                }
            }
        }
        return result;
    }
}
