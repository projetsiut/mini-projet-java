package casir.core;

import java.util.ArrayList;

public abstract class View
{
    protected ArrayList<View> siblings;

    public View()
    {
        this.siblings = new ArrayList<>();
    }

    public abstract String render();

    public void addSibling(View newSibling){
        this.siblings.add(newSibling);
    };
}
