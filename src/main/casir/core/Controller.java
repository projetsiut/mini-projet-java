package casir.core;

import casir.viewparser.ViewParser;

import java.util.Map;

public abstract class Controller
{

    public abstract String index(Map<String, String> get);

    protected View load(String viewName, Map data){
        try {
            return ViewParser.parse(viewName, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String redirect(String location){
        return "<!doctype html>" +
                    "<html>" +
                        "<head>" +
                            "<title>redirect</title>" +
                        "</head>" +
                        "<body>" +
                            "<script>window.location = \"" + location + "\"</script>" +
                        "</body>" +
                    "</html>";
    }
}
