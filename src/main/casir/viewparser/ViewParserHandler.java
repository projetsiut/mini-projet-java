package casir.viewparser;

import casir.core.View;
import casir.dataTransferObject.Student;
import casir.viewparser.element.*;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Map;

public class ViewParserHandler extends DefaultHandler
{

    private View documentView;
    private Map data;

    public ViewParserHandler(Map<String, Object> data)
    {
        this.data = data;
    }

    public void startElement(String namespaceURI, String lname, String qname, Attributes attrs)
    {
//      Only route element are useful
        if(qname.equals("document")){
            this.documentView = new DocumentView();

        } else if (qname.equals("head")){

            String title = attrs.getValue("title");
            HeadView view = new HeadView(title);

            this.documentView.addSibling(view);

        } else if (qname.equals("content")) {
            ContentView view = new ContentView();

            this.documentView.addSibling(view);

        } else if (qname.equals("title")){
            String content = attrs.getValue("content");
            TitleView view = new TitleView(content);

            this.documentView.addSibling(view);
        } else if (qname.equals("students")) {
            StudentsView view = new StudentsView((ArrayList<Student>) data.get("students"));

            this.documentView.addSibling(view);
        } else if (qname.equals("student")) {
            StudentView view = new StudentView((Student) data.get("student"));

            this.documentView.addSibling(view);
        } else if (qname.equals("add")) {
            StudentCreationView view = new StudentCreationView();

            this.documentView.addSibling(view);
        } else if (qname.equals("search")) {
            SearchView view = new SearchView();

            this.documentView.addSibling(view);
        }
    }

    public View getDocument(){
        return this.documentView;
    }

}
