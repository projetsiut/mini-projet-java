package casir.viewparser.element;

import casir.core.View;
import casir.dataTransferObject.Student;

import java.util.ArrayList;

public class StudentsView extends View
{

    private ArrayList<Student> students;

    public StudentsView(ArrayList<Student> students)
    {
        this.students = students;
    }

    @Override
    public String render()
    {
        String result = "<a href=\"http://localhost:8000/add\">Ajouter</a>"+
                        "<table>"+
                            "<thead>"+
                                "<tr><th>ID</th><th>Prenom</th><th>Nom</th><th>Groupe</th><th>Actions</th></tr>"+
                            "</thead>"+
                            "<tbody>";

        for (Student student : this.students) {
            result += StudentView.renderStudent(student);
        }

        result += "</tbody></table>";

        return result;
    }
}
