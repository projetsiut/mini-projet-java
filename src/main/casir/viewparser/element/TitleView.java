package casir.viewparser.element;

import casir.core.View;

public class TitleView extends View
{
    private String content;

    public TitleView(String content)
    {
        this.content = content;
    }


    @Override
    public String render()
    {
        return "<h1>" + this.content + "</h1>";
    }
}
