package casir.viewparser.element;

import casir.core.View;
import casir.dataTransferObject.Student;


public class StudentView extends View
{
    private Student student;

    public StudentView(Student student)
    {
        this.student = student;
    }

    @Override
    public String render()
    {
        String result = "<form action=\"edit\" method=\"get\" class=\"col s12 stripped\">\n" +
                                "<input hidden name=\"id\" type=\"text\" value=\"" + this.student.getiD() +"\">\n"+
                                "<label>\n" +
                                    "Nom" +
                                    "<input name=\"firstName\" type=\"text\" value=\"" + this.student.getFirstName() + "\" required>\n" +
                                "</label>\n" +
                                "<label>\n" +
                                    "Prénom" +
                                    "<input name=\"lastName\" type=\"text\" value=\"" + this.student.getLastName() + "\" required>\n" +
                                "</label>\n" +
                                "<label>\n" +
                                    "Groupe" +
                                    "<input name=\"group\" type=\"text\" value=\"" + this.student.getGroup() + "\" required>\n" +
                                "</label>\n" +
                                "<button class=\"btn waves-effect waves-light white-text\" type=\"submit\" name=\"action\">Enregistrer</button>\n"+
                                " <a href=\"http://localhost:8000\" class=\"btn waves-effect waves-light\">Retour</a>"+
                        "</form>\n";

        return result;
    }

    public static String renderStudent(Student student)
    {
        return "<tr>"+
                    "<td>" + student.getiD() + "</td>"+
                    "<td>" + student.getFirstName() + "</td>"+
                    "<td>" + student.getLastName() + "</td>"+
                    "<td>" + student.getGroup() + "</td>"+
                    "<td>"+
                        "<a class=\"btn waves-effect waves-light\" href=\"http://localhost:8000/edit?id=" + student.getiD() + "\">Modifier</a>" +
                        " <a class=\"btn waves-effect waves-light\" href=\"http://localhost:8000/delete?id=" + student.getiD() + "\">Supprimer</a>"+
                    "</td>" +
                "</tr>";
    }
}
