package casir.viewparser.element;

import casir.core.View;
import casir.dataTransferObject.Student;

import java.util.ArrayList;

public class SearchView extends View
{

    @Override
    public String render()
    {
        String result = "<div class=\"row\">\n" +
                            "<form method=\"get\" action=\"search\" class=\"col s12\">\n" +
                                "<div class=\"row\">\n" +
                                    "<div class=\"input-field col s6\">\n" +
                                        "<i class=\"material-icons prefix\">search</i>\n" +
                                        "<input id=\"icon_prefix\" name=\"search\" placeholder=\"Rechercher\">\n" +
                                    "</div>\n" +
                                "</div>\n" +
                                "<button type=\"submit\" hidden></button>\n" +
                            "</form>\n" +
                        "</div>\n";

        return result;
    }
}
