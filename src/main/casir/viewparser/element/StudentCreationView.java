package casir.viewparser.element;

import casir.core.View;
import casir.dataTransferObject.Student;


public class StudentCreationView extends View
{

    @Override
    public String render()
    {
        String result = "<form action=\"add\" method=\"get\">" +
                            "<label>" +
                                "Nom" +
                                "<input name=\"firstName\" type=\"text\" required>" +
                            "</label>" +
                            "<br>" +
                            "<label>" +
                                "Prénom" +
                                "<input name=\"lastName\" type=\"text\" required>" +
                            "</label>" +
                            "<br>" +
                            "<label>" +
                                "Groupe" +
                                "<input name=\"group\" type=\"text\" required>" +
                            "</label>" +
                            "<br>"+
                            "<button type=\"submit\" name=\"action\" class=\"btn waves-effect waves-light\">Enregistrer</button>"+
                            " <a href=\"http://localhost:8000\" class=\"btn waves-effect waves-light\">Retour</a>"+
                        "</form>";
        return result;
    }
}
