package casir.viewparser.element;

import casir.core.View;

public class HeadView extends View
{
    private String title;

    public HeadView(String title)
    {
        if (title == null)
            this.title = "Default Title";
        else
            this.title = title;
    }

    @Override
    public String render()
    {
        return "<head>\n"+
                 "<title>" + this.title + "</title>\n"+
                 "<link rel=\"stylesheet\" href=\"assets/css/materialize.min.css\" />\n"+
                 "<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">"+
                "</head>\n"+
                "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js\"></script>\n";
    }
}
