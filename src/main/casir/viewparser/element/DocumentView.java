package casir.viewparser.element;

import casir.core.View;

import java.util.ArrayList;

public class DocumentView extends View
{
    public DocumentView()
    {
        this.siblings = new ArrayList<>();
    }

    @Override
    public String render()
    {
        String result = "<!DOCTYPE html>\n"+
                        "<html lang=\"fr\">\n";

        for (View sibling : this.siblings) {
            result += sibling.render();
        }

        result += "</html>";

        return result;
    }

    @Override
    public void addSibling(View newSibling)
    {
        if (newSibling.getClass() == HeadView.class || newSibling.getClass() == ContentView.class){
            // juste add it to the list
            this.siblings.add(newSibling);
        } else {
            // add it to the content
            for (View sibling : this.siblings) {
                if (sibling.getClass() == ContentView.class){
                    sibling.addSibling(newSibling);
                }
            }
        }
    }
}
