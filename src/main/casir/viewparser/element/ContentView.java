package casir.viewparser.element;

import casir.core.View;

public class ContentView extends View
{
    @Override
    public String render()
    {
        String result = "<body>\n"+
                            "<div class=\"container\">\n";

        for (View sibling : this.siblings) {
            result += sibling.render();
        }

        result += "</div>\n</body>\n";

        return result;
    }
}
