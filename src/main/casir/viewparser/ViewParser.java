package casir.viewparser;

import casir.core.View;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.Map;

public class ViewParser
{

    public static View parse(String viewName, Map data) throws Exception
    {
        View documentView;

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        ViewParserHandler handler = new ViewParserHandler(data);

        parser.parse("view/" + viewName + ".xml", handler);
        documentView = handler.getDocument();

        return documentView;
    }
}
