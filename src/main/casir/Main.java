package casir;

import casir.core.server.Server;

public class Main
{

    public static void main(String[] args)
    {
        System.out.println("Start listening : http://localhost:8000");
        try {
            new Server().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
