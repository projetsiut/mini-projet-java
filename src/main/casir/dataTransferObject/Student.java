package casir.dataTransferObject;

public class Student
{

    private String iD;
    private String firstName;
    private String lastName;
    private String group;

    public Student(String iD, String firstName, String lastName, String group) {
        this.iD = iD;
        this.firstName = firstName;
        this.lastName = lastName;
        this.group = group;
    }


    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

}
