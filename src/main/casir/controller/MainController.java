package casir.controller;

import casir.core.Controller;
import casir.core.View;
import casir.dataTransferObject.Student;
import casir.expositionLayer.DatabaseHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainController extends Controller
{

    private DatabaseHandler db;
    private Map<String, String> get;

    public MainController()
    {
        this.db = DatabaseHandler.getInstance();
    }

    @Override
    public String index(Map<String, String> get)
    {
        HashMap<String, List<Student>> data = new HashMap<>();

        data.put("students", this.db.getStudentsList());

        View view = this.load("index", data);
        return view.render();
    }

    /**
     * Home page
     * @return String - The page
     */
    public String add(Map<String, String> get){
        String action = get.get("action");

        if (action != null){
            String id = this.db.nextID();
            String firstName = get.get("firstName");
            String lastName = get.get("lastName");
            String group = get.get("group");

            Student student = new Student(id, firstName, lastName, group);

            this.db.addStudent(student);

            return this.redirect("/");
        } else {
            View view = this.load("add", new HashMap());
            return view.render();
        }
    }

    /**
     * Edition page
     * @param get Map containing GET parameters
     * @return String
     */
    public String edit(Map<String, String> get){
        HashMap<String, Object> data = new HashMap();

        String studentId = get.get("id");
        String action = get.get("action");

        if (action != null){
            String id = get.get("id");
            String firstName = get.get("firstName");
            String lastName = get.get("lastName");
            String group = get.get("group");

            Student student = new Student(id, firstName, lastName, group);
            this.db.modifyStudent(student);

            return this.redirect("/");
        } else if(studentId != null){ // display form
            data.put("student", this.db.getStudentById(studentId));

            View view = this.load("edit", data);
            return view.render();
        } else {

            return this.index(get);
        }
    }

    /**
     * Delete user
     * @param get GET parameter
     * @return --
     */
    public String delete(Map<String, String> get){
        String studentId = get.get("id");

        if (studentId != null){
            this.db.removeStudent(studentId);
        }

        return this.redirect("/");
    }

    /**
     * Display students which match search string
     * @param get GET parameter
     * @return --
     */
    public String search(Map<String, String> get){
        HashMap<String, List<Student>> data = new HashMap<>();
        ArrayList<Student> studentsList = (ArrayList<Student>) this.db.getStudentsList();
        ArrayList<Student> studentsResult = new ArrayList<>();

        String searchedString = get.get("search");
        searchedString = "(.*)" + searchedString + "(.*)";

        for (Student student : studentsList) {
            if (student.getFirstName().toLowerCase().matches(searchedString.toLowerCase())
                    || student.getLastName().toLowerCase().matches(searchedString.toLowerCase())
                    || student.getGroup().toLowerCase().matches(searchedString.toLowerCase())){
                studentsResult.add(student);
            }
        }

        data.put("students", studentsResult);

        View view = this.load("index", data);
        return view.render();
    }
}
