package casir.expositionLayer;

import casir.dataTransferObject.Student;
import casir.core.readers.StudentDatabaseReader;
import casir.core.writers.StudentWriter;

import java.util.List;

public class DatabaseHandler
{
    private final static DatabaseHandler INSTANCE = new DatabaseHandler();
    private List<Student> db;
    private int newID;

    private DatabaseHandler()
    {
        this.db = StudentDatabaseReader.ReadStudents();
        int id = 1;
        for (Student stud:this.db)
        {
            if(Integer.parseInt(stud.getiD()) > id)
            {
                id = Integer.parseInt(stud.getiD())+1;
            }
        }
        newID = id;
    }

    public static DatabaseHandler getInstance()
    {
        return INSTANCE;
    }


    public void addStudent(Student studentToAdd)
    {
        for (Student currentStud:this.db)
        {
            if(currentStud.getiD() == studentToAdd.getiD())
            {
                return;
            }
        }

        this.db.add(studentToAdd);
        this.newID++;

        this.writeStudents();
    }

    public void removeStudent(String studentToRemoveID)
    {
        Student toRemove = null;
        for (Student currentStud:this.db)
        {
            if(currentStud.getiD().equals(studentToRemoveID))
            {
                toRemove = currentStud;
                break;
            }
        }

        if (toRemove != null)
        {
            this.db.remove(toRemove);
        }

        this.writeStudents();
    }

    public List<Student> getStudentsList()
    {

        return this.db;
    }

    public void modifyStudent(Student studentToModify)
    {
        this.removeStudent(studentToModify.getiD());
        this.addStudent(studentToModify);
    }

    public Student getStudentById(String id){
        for (Student currentStudent : this.getStudentsList()) {
            if (currentStudent.getiD().equals(id))
                return currentStudent;
        }
        return null;
    }

    public String nextID()
    {
        System.err.println("ici");
        return ""+this.newID;
    }

    public void writeStudents()
    {
        StudentWriter.WriteStudents(this.db);
    }
}
